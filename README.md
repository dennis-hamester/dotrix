Dotrix, the pedantic dotfiles manager
=====================================

Dotrix keeps track of all you dotfiles. Just organize them in one central place and describe where
the symlinks should go in some `toml` files. Dotrix will take care of the rest.

Linux users tend to be quite pedantic. Everything has to be perfect. Dotrix is no different. It will
warn about everything suspicious it finds. Created a new file without telling dotrix where to place
the symlink? No problem, dotrix will complain. If it doesn't, you can be sure that all your precious
dotfiles are in place and properly symlinked to. A full list of warnings is provided
[here](#warnings).

Keep in mind that dotrix is in early development. There have not been many tests so far. It may very
well eat your dotfiles. You have been warned.


Installation
------------
Dotrix is written in stable rust and built with cargo. Download the [latest release][release] or
clone the [repository][repo]. To build dotrix, just run:

```shell
cargo build --release
```

The binary will be in the `target/release` directory.

There is no `make install` equivalent at the moment. You have to manually copy the `dotrix` binary,
usually to `/usr/local/bin`, or `/usr/bin` if your are packaging dotrix.

[release]: https://gitlab.com/dennis-hamester/dotrix/tags
[repo]: https://gitlab.com/dennis-hamester/dotrix.git

### Archlinux
You can install dotrix from the AUR. Two packages are available:

* [dotrix](https://aur.archlinux.org/packages/dotrix/)
* [dotrix-git](https://aur.archlinux.org/packages/dotrix-git/)


Usage
-----

```
Usage: dotrix [options]

Options:
    -h --help           print this help
    --src DIR           source directory; defaults to '~/dotfiles/'
    --dst DIR           destination directory; defaults to '~/'
    -d --dryrun         don't create any symlinks
    --version           show dotrix version
```

For a real world example, have a look at my [personal dotfiles][dotfiles].

Dotrix assumes that your dotfiles are organized into several directories, each usually corresponding
to one application. It looks for a `dot.toml` file inside each directory. If you're not familiar
with `toml`, don't worry, it is a very simple format (somewhat like `json` and `yaml`). The
specification can be found [here][toml].

A typical directory might look like this:

```shell
$ ls -R
.:
i3  mpd  zsh

./i3:
config  dot.toml

./mpd:
dot.toml  mpd.conf

./zsh:
dot.toml  zprofile  zshenv  zshrc
```

Here is the `dot.toml` from the `zsh` sub-directory:

```toml
[dotrix.link]
".zshrc" = "zshrc"
".zshenv" = "zshenv"
".zprofile" = "zprofile"
```

As you can see, `dot.toml` files have one top-level table called `dotrix`. It is optional, but
dotrix will warn if it is not present. After all, an empty `dot.toml` doesn't make a lot of
sense. The `dotrix` table can have two children:

* `ignore`
* `link`

### `dotrix.ignore`
If you have files that no symlink points to, dotrix will warn about them. This ensures that you
don't forget about newly created files. To silence the warning, you can list the file in the
`ignore` list:

```toml
[dotrix]
ignore = [ "some_file", "some_other_file.conf" ]
```

File names are relative to the `dot.toml` file.

### `dotrix.link`
This is probably the most important part of a `dot.toml` file, where symlinks are
specified. `dotrix.link` is a table, that maps symlink names to files relative to the current
`dot.toml`.


```toml
[dotrix.link]
".zshrc" = "zshrs"
".config/compton.conf" = "compton.conf"
```

Key values are interpreted relative to the destination directory (default: `~/`, override with
`--dst`). Keys must be unique. You can not create a symlink that points to two different files. This
rule applies across all `dot.toml` files! But don't worry, dotrix will complain if it finds
ambiguities.

[dotfiles]: https://gitlab.com/dennis-hamester/dotfiles
[toml]: https://github.com/toml-lang/toml

### Testing your `dot.toml` files
You can pass the `--dryrun` (or `-d`) switch to perform a simulation. Dotrix will produce the same
output without any changes to the filesystem.


Warnings
--------
Since v0.1.0:

* Missing `dotrix` table.
* Ignoring a file multiple times.
* Multiple destinations for a single symlink.
* Specifying links for ignored files.
* Unknown files.
* Existing symlinks with different destinations.
* Non-existent symlink destinations.
* Various other filesystem checks.


Planned Features
----------------
* Disable `dot.toml` files.
* Create directories up the a symlink.
* Configuration file. Mainly for overriding `--src`.
* Ever-increasing pedantry.


License
-------
```
Copyright (c) 2015, Dennis Hamester dennis.hamester@startmail.com

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
